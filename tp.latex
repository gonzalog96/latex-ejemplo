\documentclass{article}
\usepackage{caption}
\usepackage{geometry}
 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=15mm,
 bottom=15mm
 }
\usepackage[utf8]{inputenc}
\usepackage{graphicx}

\title{Trabajo práctico de contingencia - Matemática}
\author{E.E.S. N*6 - San Isidro}
\date{Matías Braun - 5to 3ra - marzo del 2020}

\begin{document}

\maketitle
\section{Resuelva los siguientes ejercicios combinados:}
\begin{enumerate}
    \item $\sqrt{16+9} + (2+3)^3=$\\$\sqrt{25} + 5^3=$\\$5 + 125=$\\$\textbf{130.}$
    \item $2^4:2^3 - \sqrt{49} - 8^0=$\\$2 - 7 - 1=$\\$\textbf{-6.}$
    \item $\sqrt[3]{8} * 3^3 - (6^2 - 6) \div [7^2 - (11 - \sqrt[3]{27})^2]=$\\$2 * 27 - (36 - 6) \div [49 - (11 - 3)^2]=$\\$54 - 30 \div [49 - 64]=$\\$24 \div (-15)=$\\$-\frac{\textbf{8}}{\textbf{5}}.$
    \item $(-3)^2\div(-3)^5-(12^2-6)\div[0^6+3*(3^3-\sqrt[3]{64})] =$\\$(-3)^{-3} - (144 - 6) \div [3 * (27 - 4)] =$\\$-\frac{1}{27} - 138 \div (3 * 23) =$\\$-\frac{3727}{27} \div 69 =$\\$-\frac{3727}{27} * \frac{1}{69} =$\\$-\frac{\textbf{3727}}{\textbf{1863}}.$
\end{enumerate}

\section{Resuelva las siguientes ecuaciones: }
\begin{enumerate}
    \item $4t-2=8+3t =$\\$4t - 3t = 8 + 2$\\$\textbf{t = 10.}$
    \item $(3h-1)+7=8h-(3-2h) =$\\$(3h-1)+(3-2h)-8h=-7$\\$3h-2h-8h=-7+1-3$\\$7h=-9$\\$h=\frac{-9}{-7}$\\$\textbf{h=}\frac{\textbf{9}}{\textbf{7}}$.
    \item 
    $3-(8v-5)+(6-7v)-1=7-(v-1)+(4v+4)$\\
    $-(8v-5)+(6-7v)+(v-1)-(4v+4)=7+1-3$\\
    $-8v+5+6-7v+v-1-4v-4=5$\\
    $-8v-7v+v-4v=5+4+1-6-5$\\
    $-18v=-1$\\
    $\textbf{v=}\frac{\textbf{1}}{\textbf{18}}.$\\
    \item
    $\frac{27+d}{4}=3+d$\\
    $27+d=(3+d)*4$\\
    $27+d=12+4d$\\
    $27-12=4d-d$\\
    $15=3d$\\
    $\textbf{5=d}.$\\
    \item
    $\frac{e-5}{9}=\frac{e-25}{5}$\\
    $e-5=\frac{e-25}{5}*9$\\
    $e-5=\frac{9e-225}{5}$\\
    $e=\frac{9e-225}{5}+5$\\
    $e=\frac{9e}{5}-45+5$\\
    $e=\frac{9e}{5}-40$\\
    $e-\frac{9e}{5}=-40$\\
    $-\frac{4}{5}e=-40$\\
    $\textbf{e=50}.$\\
    \item
    $j-\frac{2+j}{6}=\frac{1}{2}$\\
    $\frac{6j-(2+j)}{6} = \frac{1}{2}$\\
    $\frac{6j-2-j}{6}=\frac{1}{2}$\\
    $\frac{5j-2}{6} = \frac{1}{2}$\\
    $\frac{5}{6}j-\frac{1}{3} = \frac{1}{2}$\\
    $\frac{5}{6}j = \frac{1}{2} + \frac{1}{3}$\\
    $\frac{5}{6}j = \frac{5}{6}$\\
    $\textbf{j=1}.$\\
    \item
    $\frac{k-32}{40}=k+7$\\
    $k-32 = (k+7) * 40$\\
    $k-32 = 40k + 280$\\
    $-32-280 = 40k-k$\\
    $-312=39k$\\
    $\textbf{-8=k}.$\\
    \item
    $7x^2-x=2x-x^2$\\
    $7x^2+x^2 = 2x + x$\\
    $8x^2 = 3x$\\
    $8x^2 - 3x = 0$\\
    $x(8x-3) = 0$\\
    $8x - 3 = 0$\\
    $\textbf{x}_{1} \textbf{= 0}.$\\
    $\textbf{x}_{2} \textbf{=} \frac{\textbf{3}}{\textbf{8}}.$\\
    \item
    $x(8x-3)=0$\\
    $8x - 3 = 0$\\
    $\textbf{x}_{1} \textbf{= 0}$.\\
    $\textbf{x}_{2} \textbf{=} \frac{\textbf{3}}{\textbf{8}}$.\\
    \item
    $8x^2-3x=0$\\
    $x(8x-3) = 0$\\
    $8x - 3 = 0$\\
    $\textbf{x}_{1} \textbf{= 0}$.\\
    $\textbf{x}_{2} = \frac{\textbf{3}}{\textbf{8}}$.\\
\end{enumerate}
\newpage
\section{Graficar las siguientes funciones lineales: }
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{3x+9.PNG}
    \includegraphics[width=3cm, height=7cm]{3x+9-tabla.PNG}
    \caption*{y = 3x + 9}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{-7x+14.PNG}
    \includegraphics[width=3cm, height=7cm]{-7x+14-tabla.PNG}
    \caption*{y = -7x + 14}
\end{figure}
\newpage
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{-5x-10.PNG}
    \includegraphics[width=3cm, height=7cm]{-5x-10-tabla.PNG}
    \caption*{y = -5x - 10}
\end{figure}
\\
\section{Hallar el dominio de las siguientes funciones: }
\begin{enumerate}
    \item 
    $\frac{x-4}{6+2x}$\\
    Observar que el dominio $Dom$ son \textbf{todos los reales $R$ menos los valores que anulan al denominador.} Es decir: \\
    $6+2x \neq 0$\\
    $2x \neq -6$\\
    $x \neq -3$\\
    Respuesta: $Dom = R - \lbrace-3\rbrace$\\
    \item
    $\sqrt{x+2}$\\
    Observar que \textbf{el radicando no puede ser negativo} (dentro del conjunto de los números reales). Es decir:
    $x+2 \ge 0$\\
    $x \ge -2$\\
    Respuesta: $Dom = [-2; +\infty]$
\end{enumerate}
\\
\section{Halle las raíces y la ordenada al origen de las siguientes funciones: }
\begin{enumerate}
    \item 
    $y = x^2 - x$\\
    Igualamos a cero para hallar las raíces/ceros:\\
    $x^2 - x = 0$\\
    Factorizamos:\\
    $x(x - 1) = 0$\\
    Entonces $x_{1} = 0$. Verificamos ahora $x-1$:\\
    $x-1 = 0$\\
    $x = 1$\\
    Por lo tanto $x_{2} = 1$. Luego $C_{0} = \lbrace0,1\rbrace$.\\
    Ordenada al origen: evaluamos a la función en $x = 0$. Luego $O.O.: (0,0).$
    \item
    $y=x^2-9$\\
    Despejamos $x^2$ para hallar las raíces:\\
    $x^2 = 9$\\
    $x = \pm\sqrt{9}$\\
    Obtenemos de esta forma las raíces:
    $x_{1} = 3$,
    $x_{2} = -3$.\\
    Luego $C_0 = \lbrace-3,3\rbrace$.
    \\Ordenada al origen: $(0,-9)$.
    \item
    $y = (x+2)^2$\\
    Observar que si $x = -2$ entonces el conjunto, en su totalidad, es cero. Luego $x_1 = 0$.\\
    Si se desarrolla el producto:\\
    $(x+2) (x+2)$\\
    Se puede observar que ambos miembros se anulan cuando $x = -2$. Luego, se tiene una raíz única/doble. Entonces $C_{0} = \lbrace-2\rbrace$.
    Ordenada al origen: $(0,4)$.
    \item
    $y = -5(x+1)(x-2)$\\
    Observar que esta función se encuentra en su \textbf{forma factorizada. Por lo tanto, es posible ver sus raíces}:\\
    $x-1=0$\\
    $x_{1} = 1$\\
    $x-2=0$\\
    $x_{2}=2$\\
    Luego $C_0=\lbrace1,2\rbrace$. Ordenada al origen: $(0,10)$.
    \item
    $y = 3x^2 + 12x -9$\\
    Esta función se encuentra en su forma polinómica. Utilizaremos la \textbf{fórmula resolvente} para obtener las raíces.\\
    $x_{1,2} = \frac{-b \pm \sqrt{b^2-4*a*c}}{2*a}$ con $a = 3, b = 12, c=-9$.\\
    $x_{1,2} = \frac{-12 \pm \sqrt{12^2-4*3*(-9)}}{6}$\\
    $x_{1,2} = \frac{-12 \pm \sqrt{252}}{6}$\\
    $x_{1} = \frac{-12 + 2*\sqrt{63}}{6} = -2 + \frac{1}{3}\sqrt{63}$\\
    $x_{2} = \frac{-12 - 2*\sqrt{63}}{6} = -2 - \frac{1}{3}\sqrt{63}$\\
    Por lo tanto, se concluye que $C_{0} = \lbrace-2 + \frac{1}{3}\sqrt{63}, -2 - \frac{1}{3}\sqrt{63}\rbrace$. Ordenada al origen en: $(0,-9)$.
    \item
    $y = -x^2 + x$\\
    Factorizamos la expresión (factor común): \\
    $x(-x+1)$\\
    Por lo tanto, es obvio que una de las raíces es 0. Verifiquemos la expresión interna del paréntesis:\\
    $-x + 1 = 0$\\
    $-x = -1$\\
    $x = 1$\\
    En conclusión, $C_{0} = \lbrace0, 1\rbrace$. Ordenada al origen (evaluando en $x = 0$) en: $(0,0)$.\\
    \item
    $y = 2x^2+x-3$\\
    Usemos nuevamente la fórmula resolvente: \\
    $x_{1,2} = \frac{-1 \pm \sqrt{1^2 - 4 * 2 * (-3)}}{4}$\\
    $x_{1,2} = \frac{-1 \pm \sqrt{25}}{4}$\\
    $x_{1} = 1$\\
    $x_{2} = -\frac{3}{2}$\\
    Entonces $C_{0} = \lbrace-\frac{3}{2},1\rbrace$. Ordenada al origen en $(0, -3)$.
\end{enumerate}
\newpage
\section{Grafique las funciones del punto 5 con todos los elementos necesarios:}
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico1.PNG}
    \caption*{$f(x) = x^2-x$
    \\$c_{0}=\lbrace0,1\rbrace$
    \\$Ordenada: (0,0)$
    \\$Concavidad: a > 0 \rightarrow 	 \bigcup$
    \\$Vértice: (\frac{1}{2}, -\frac{1}{4})$}
    \\Eje de simetría en $x = \frac{1}{2}$
\end{figure}
\newline
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico2.PNG}
    \caption*{$g(x) = x^2-9$
    \\$c_{0}=\lbrace-3, 3\rbrace$
    \\$Ordenada: (0,-9)$
    \\$Concavidad: a > 0 \rightarrow 	 \bigcup$
    \\$Vértice: (0,-9)$}
    \\Eje de simetría en $x = 0$
\end{figure}
\newpage
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico3.PNG}
    \caption*{$h(x) = (x+2)^2$
    \\$c_{0}=\lbrace-2\rbrace$
    \\$Ordenada: (0,4)$
    \\$Concavidad: a > 0 \rightarrow 	 \bigcup$
    \\$Vértice: (-2, 0)$}
    \\Eje de simetría en $x = -2$
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico4.PNG}
    \caption*{$i(x) = -5(x+1)(x-2)$
    \\$c_{0}=\lbrace-1,2\rbrace$
    \\$Ordenada: (0,10)$
    \\$Concavidad: a < 0 \rightarrow 	 \bigcap$
    \\$Vértice: (\frac{1}{2}, \frac{45}{4})$}
    \\Eje de simetría en $x = \frac{1}{2}$
\end{figure}
\newpage
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico5.PNG}
    \caption*{$j(x) = 3x^2+12x-9$
    \\$c_{0}=\lbrace-2\pm\frac{1}{3}\sqrt{63}\rbrace$
    \\$Ordenada: (0,-9)$
    \\$Concavidad: a > 0 \rightarrow 	 \bigcup$
    \\$Vértice: (-2,-21)$}
    \\Eje de simetría en $x = -2$
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico6.PNG}
    \caption*{$k(x) = -x^2+x$
    \\$c_{0}=\lbrace0,1\rbrace$
    \\$Ordenada: (0,0)$
    \\$Concavidad: a < 0 \rightarrow 	 \bigcap$
    \\$Vértice: (\frac{1}{2},\frac{1}{4})$}
    \\Eje de simetría en $x = \frac{1}{2}$
\end{figure}
\newpage
\begin{figure}[h]
    \centering
    \includegraphics[width=7cm, height=7cm]{grafico7.PNG}
    \caption*{$l(x) = 2x^2+x-3$
    \\$c_{0}=\lbrace-\frac{3}{2},1\rbrace$
    \\$Ordenada: (0,-3)$
    \\$Concavidad: a > 0 \rightarrow 	 \bigcup$
    \\$Vértice: (-\frac{1}{4},-\frac{25}{8})$}
    \\Eje de simetría en $x = -\frac{1}{4}$
\end{figure}
\section{A partir del siguiente triángulo rectángulo, complete con las respuestas los ítems: }
\begin{figure}[h]
    \centering
    \includegraphics[width=3cm, height=3cm]{triangulo.PNG}
    \caption*{$sen A = a/c\\
    cos A = b/c\\
    tg A = a/b\\
    sen B = b/c\\
    cos B = a/c\\
    tg B = b/a$}
\end{figure}
\end{document}
